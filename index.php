<!DOCTYPE html>
<html>
<head>
    <style>
        html{
            font-size: 14px;
        }
    </style>
</head>
<body>
    <div id="title">
    <?php
    $con = mysqli_connect('localhost:3306','root','','mydb');
    if (!$con) {
        die('Could not connect: ' . mysqli_error($con));
    }

    mysqli_select_db($con,"mydb");
    $sql="SELECT * FROM csgoitem WHERE sellDate IS NULL ORDER BY currentPrice DESC";
    $result = mysqli_query($con,$sql);

    $sqlComodities = "SELECT * FROM commodities ORDER BY price DESC";
    $resultComodities = mysqli_query($con,$sqlComodities);

    if(mysqli_num_rows($result) > 4){
        echo "[STORE] ";
        if($row = mysqli_fetch_array($result)) echo $row['titleSlug'];
        while($row = mysqli_fetch_array($result)) {
            if($row['titleSlug'] != NULL) echo ", " . $row['titleSlug'];
        }
        while($row = mysqli_fetch_array($resultComodities)) {
            if($row['commodityName'] != NULL) echo ", " . $row['amount'] . " " . $row['commodityName'] . "s";
        }
    } else {
        $prices = " [W]";
        echo "[H] ";
        if($row = mysqli_fetch_array($result)){
            echo $row['titleSlug'];
            if($row['tradeableDate'] == NULL){
                $prices = $prices . " $" . $row['currentPrice'];
            }
            else {
                $prices = $prices . " " . $row['currentPrice'] . "k";
            }
        }
        while($row = mysqli_fetch_array($result)) {
            if($row['titleSlug'] != NULL) echo ", " . $row['titleSlug'];
            if($row['tradeableDate'] == NULL){
                $prices = $prices . ", $" . $row['currentPrice'];
            }
            else {
                $prices = $prices . ", " . $row['currentPrice'] . "k";
            }
        }
        while($row = mysqli_fetch_array($resultComodities)) {
            if($row['commodityName'] != NULL) echo ", " . $row['amount'] . " " . $row['commodityName'] . "s";
            if($row['price'] != NULL) $prices = $prices . ", " . $row['price'] . "$ per " . $row['commodityName'];
        }
        echo $prices;
    }

    ?>
    </div>
    <button onclick="copyToClip('title')">Copy</button>
    <br><br>
    <div id="text">
    Open for all offers, but no lowballs please.&lt;br&gt;<br>
    &lt;hr&gt;<br>
    <?php

    $result->data_seek(0);

    while($row = mysqli_fetch_array($result)) {
        if($row['isKnife'] == true) echo "★ ";
        if($row['name'] != NULL) echo $row['name'] . '&lt;br&gt;' . "<br>";
        if($row['info'] != NULL) echo $row['info'] . '&lt;br&gt;' . "<br>";
        if($row['csdeals'] != NULL) echo  "[CSD Screenshot](" . $row['csdeals'] . ')&lt;br&gt;' . "<br>";
        if($row['csmoney'] != NULL) echo "[CSM Screenshot](" . $row['csmoney'] . ')&lt;br&gt;' . "<br>";
        if($row['inGame'] != NULL) echo "[In-Game Screenshot](" . $row['inGame'] . ')&lt;br&gt;' . "<br>";
        if($row['currentPrice'] != " "){
            if($row['tradeableDate'] == NULL){
                echo "Buyout: $" . $row['currentPrice'] . ' on Bitskins &lt;br&gt;' . "<br>";
            }
            else{
                $now  = time();
                $tradeableDate = strtotime($row['tradeableDate']);
                $dateDelta =  $tradeableDate - $now;
                $dateDelta = round($dateDelta / (60 * 60 * 24));
                if($dateDelta <= 0){
                    echo "Tradeable" . '&lt;br&gt;' . "<br>";
                }
                else {
                    $days = " days";
                    if($dateDelta == 1) $days = " day";
                    echo "Tradeable in " . $dateDelta . $days . '&lt;br&gt;' . "<br>";
                }
                echo "Buyout: " . $row['currentPrice'] . ' keys&lt;br&gt;' . "<br>";
            }
        }
        echo "&lt;hr&gt<br>";
    }

    $resultComodities->data_seek(0);

    while($row = mysqli_fetch_array($resultComodities)) {
        if($row['amount'] != NULL) echo $row['amount'];
        if($row['commodityName'] != NULL) echo " " . $row['commodityName'] . 's&lt;br&gt;' . "<br>";
        if($row['price'] != NULL) echo "Buyout:  " . $row['price'] . "$ per " . $row["commodityName"] . " in cash or item offers" . '&lt;br&gt;' . "<br>";
        echo "&lt;hr&gt<br>";
    }

    mysqli_close($con);
    ?>
    [Trade offer link](https://steamcommunity.com/tradeoffer/new/?partner=88701720&token=woklliDX)
    </div>
    <button onclick="copyToClip('text')">Copy</button>

    <br><br>
    <a target="_blank" href="https://ifttt.com/applets/A59v4Kmb-csgo-trade-post/edit"><button>Edit IFTTT</button></a>

    <br><br>
    <a href="add.php"><button>Add new item</button></a>

    <br><br>
    <a href="sold.php"><button>Item sold</button></a>

    <br><br>
    <a href="edit.php"><button>Edit Items</button></a>

    <script>
        function copyToClip(id) {
        var elm = document.getElementById(id);
        // for Internet Explorer

        if(document.body.createTextRange) {
            var range = document.body.createTextRange();
            range.moveToElementText(elm);
            range.select();
            document.execCommand("Copy");
        }
        else if(window.getSelection) {
            // other browsers
            var selection = window.getSelection();
            var range = document.createRange();
            range.selectNodeContents(elm);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("Copy");
        }
        }
    </script>
</body>
</html>