<!DOCTYPE html>
<html>
<body>
<?php
$con = mysqli_connect('localhost:3306','root','','mydb');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

mysqli_select_db($con,"mydb");
$sql="SELECT * FROM CsgoItem WHERE sellDate IS NULL ORDER BY currentPrice DESC";
$result = mysqli_query($con,$sql);

while($row = mysqli_fetch_array($result)) {
    if($row['isKnife'] == true) echo "★ ";
    if($row['name'] != NULL) echo $row['name'] . '&lt;br&gt;' . "<br>";
    if($row['info'] != NULL) echo $row['info'] . '&lt;br&gt;' . "<br>";
    if($row['csdeals'] != NULL) echo  "[CSDeals SS](" . $row['csdeals'] . ')&lt;br&gt;' . "<br>";
    if($row['csmoney'] != NULL) echo "[CSMoney SS](" . $row['csmoney'] . ')&lt;br&gt;' . "<br>";
    if($row['currentPrice'] != " "){
        if($row['tradeableDate'] == NULL){
            echo "Buyout: $" . $row['currentPrice'] . ' on Bitskins &lt;br&gt;' . "<br>";
        }
        else{
            $now  = time();
            $tradeableDate = strtotime($row['tradeableDate']);
            $dateDelta =  $tradeableDate - $now;
            $dateDelta = round($dateDelta / (60 * 60 * 24));
            if($dateDelta <= 0){
                echo "Tradeable" . '&lt;br&gt;' . "<br>";
            }
            else {
                $days = " days";
                if($dateDelta == 1) $days = " day";
                echo "Tradeable in " . $dateDelta . $days . '&lt;br&gt;' . "<br>";
            }
            echo "Buyout: " . $row['currentPrice'] . 'keys &lt;br&gt;' . "<br>";
        }
    }
    echo '<input placeholder="Sale price" type="number" id=inn' . $row['itemId'] . ' step="0.0000001"><br>';
    echo '<input placeholder="Sale type" type="text" id=type' . $row['itemId'] . '><br>';
    echo '<button onclick="sold(' . $row['itemId'] . ')">Sold!</button><br>';
    echo "&lt;hr&gt<br>";
}

mysqli_close($con);
?>

<br><br>
<a href="index.php"><button>Home</button></a>
</body>
<script>
    function sold(id){
        var price = document.getElementById("inn" + id).value;
        var type = document.getElementById("type" + id).value;
        window.location.replace("soldActionPage.php?itemId=" + id + "&sellPrice=" + price + "&saleType=" + type);
    }
</script>
</html>