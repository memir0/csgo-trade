-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 29, 2019 at 04:17 PM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `csgoitem`
--

DROP TABLE IF EXISTS `csgoitem`;
CREATE TABLE IF NOT EXISTS `csgoitem` (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `titleSlug` text NOT NULL,
  `info` text,
  `float` float DEFAULT NULL,
  `csdeals` varchar(45) DEFAULT NULL,
  `csmoney` varchar(45) DEFAULT NULL,
  `inGame` varchar(45) NOT NULL,
  `buyPrice` float DEFAULT NULL,
  `buyDate` date DEFAULT NULL,
  `currentPrice` float NOT NULL,
  `sellPrice` float DEFAULT NULL,
  `sellDate` date DEFAULT NULL,
  `saleType` varchar(45) DEFAULT NULL,
  `tradeableDate` date DEFAULT NULL,
  `isKnife` tinyint(1) NOT NULL,
  `priceDelta` float DEFAULT NULL,
  PRIMARY KEY (`itemId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
