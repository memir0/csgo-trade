<?php
if(isset($_GET["name"])){
    $servername = "localhost:3306";
    $username = "root";
    $password = "";
    $dbname = "mydb";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // prepare and bind
    $stmt = $conn->prepare('INSERT INTO `CsgoItem` (`name`, `titleSlug`, `info`, `float`, `csdeals`, `csmoney`, `buyPrice`, `buyDate`, `currentPrice`, `isKnife`, `tradeableDate`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
    $stmt->bind_param("sssdssdsdis", $name, $titleSlug, $info, $float, $csdeals, $csmoney, $buyPrice, $buyDate, $currentPrice, $isKnife, $tradeableDate);
    
    if(isset($_GET["name"])) $name = $_GET["name"];
    if(isset($_GET["titleSlug"])) $titleSlug = $_GET["titleSlug"];
    if(isset($_GET["info"])) $info = $_GET["info"];
    if(isset($_GET["float"])) $float = $_GET["float"];
    if(isset($_GET["csdeals"])) $csdeals = $_GET["csdeals"];
    if(isset($_GET["csmoney"])) $csmoney = $_GET["csmoney"];
    if(isset($_GET["buyPrice"])) $buyPrice = $_GET["buyPrice"];
    if(isset($_GET["buyDate"])) $buyDate = date($_GET["buyDate"]);
    if(isset($_GET["currentPrice"])) $currentPrice =  $_GET["currentPrice"];
    if(isset($_GET["isKnife"])) $isKnife = 1;
    else $isKnife = 0;
    if(isset($_GET["tradeableDate"]) && $_GET["tradeableDate"] != "") $tradeableDate = date($_GET["tradeableDate"]);
    else $tradeableDate = NULL;

    echo $name . "<br>";
    echo $titleSlug . "<br>";
    echo $info . "<br>";
    echo $float . "<br>";
    echo $csdeals . "<br>";
    echo $csmoney . "<br>";
    echo $buyPrice . "<br>";
    echo $buyDate . "<br>";
    echo $currentPrice . "<br>";
    echo $isKnife . "<br>";
    echo $tradeableDate . "<br>";

    if (!$stmt->execute()) {
        echo 'error executing statement: ' . $stmt->error;
    }
    $stmt->close();
    $conn->close();
    
    echo "New records created successfully";
}
?>

<a href="index.php"><button>Home</button></a>